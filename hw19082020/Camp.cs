﻿namespace hw19082020
{
    public class Camp
    {

        //private readonly int _id;
        //public int Latitude { get; private set; }
        //public int Longitude { get; private set; }
        //public int NumberOfPeople { get; private set; }
        //public int NumberOfTents { get; private set; }
        //public int NumberOfFleshLights { get; private set; }
        //private static int _lastCampld = 0;

        //ETGAR 
        private int _id;

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public int Latitude { get;  set; }
        public int Longitude { get;  set; }
        public int NumberOfPeople { get;  set; }
        public int NumberOfTents { get;  set; }
        public int NumberOfFleshLights { get;  set; }
        private static int _lastCampld = 0;

        public Camp()
        {
            
        }
        public Camp(int latitude, int longitude, int numberOfPeople, int numberOfTents, int numberOfFleshLights )
        {
            _lastCampld += 1;
            //_id = _lastCampld;
            ID = _lastCampld;
            Latitude = latitude;
            Longitude = longitude;
            NumberOfPeople = numberOfPeople;
            NumberOfTents = numberOfTents;
            NumberOfFleshLights = numberOfFleshLights;
        }

        public static bool operator ==(Camp a , Camp b)
        {
            if (a is null && b is null)
            {
                return true;
            }
            else if(a is null || b is null)
            {
                return false; 
            }
            return a._id == b._id;
        }

        public static bool operator !=(Camp a, Camp b)
        {
            return !(a._id == b._id);
        }

        public static bool operator >(Camp a, Camp b)
        {
            if (a is null || b is null)
            {
                return false;
            }
            return a.NumberOfPeople > b.NumberOfPeople;
        }

        public static bool operator <(Camp a, Camp b)
        {
            if (a is null || b is null)
            {
                return false;
            }
            return a.NumberOfPeople < b.NumberOfPeople;
        }

        public static bool operator <=(Camp a, Camp b)
        {
            return (a == b || a < b);
        }

        public static bool operator >=(Camp a, Camp b)
        {
            return (a == b || a > b);
        }

        public static Camp operator +(Camp a, Camp b)
        {
            return new Camp(a.Latitude,a.Longitude,a.NumberOfPeople+b.NumberOfPeople,a.NumberOfTents+b.NumberOfTents, a.NumberOfFleshLights + b.NumberOfFleshLights); 
        }

        public override bool Equals(object obj)
        {
            return this == obj as Camp;
        }

        public override int GetHashCode()
        {
            return this._id;
        }

        public override string ToString()
        {
            return $"{nameof(ID)}: {_id}, {nameof(Latitude)}: {Latitude}, {nameof(Longitude)}: {Longitude}, {nameof(NumberOfPeople)}: {NumberOfPeople}, {nameof(NumberOfTents)}: {NumberOfTents}, {nameof(NumberOfFleshLights)}: {NumberOfFleshLights}";
        }
    }
}
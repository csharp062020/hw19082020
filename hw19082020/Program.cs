﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace hw19082020
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp a = new Camp(1,2,3,4,5);
            Camp b = new Camp(1,2,3,4,5);
            Camp c = a+b;
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine($"{a==c}");
            Console.WriteLine($"{Equals(a,c)}");

            object.ReferenceEquals()
            Console.WriteLine();
            Console.WriteLine("-----etgar------");
            string path = @"..\..\Camp.xml";
            CampXmlSerializer(a,path);
            Camp resCamp1 = null;
            Camp resCamp2 = null;

            resCamp1 = camXmlDeSerializer(path);
            resCamp2 = camXmlDeSerializer(path);

            Console.WriteLine($"a==resCamp: {a==resCamp1}");
            Console.WriteLine($"resCamp1==resCamp2: {resCamp1 == resCamp2}");

            Console.WriteLine($"a:       {a}");
            Console.WriteLine($"resCamp1:{resCamp1}");
            Console.WriteLine($"resCamp2:{resCamp2}");

            Console.WriteLine($"a.GetHashCode():        {a.GetHashCode()}");
            Console.WriteLine($"resCamp1.GetHashCode(): {resCamp1.GetHashCode()}");
            Console.WriteLine($"resCamp2.GetHashCode(): {resCamp2.GetHashCode()}");

        }

        private static Camp camXmlDeSerializer(string path)
        {
            Camp resCamp;
            XmlSerializer camXmlSerializer = new XmlSerializer(typeof(Camp));
            using (Stream xmlFileStream = new FileStream(path, FileMode.Open))
            {
                resCamp = camXmlSerializer.Deserialize(xmlFileStream) as Camp;
            }

            return resCamp;
        }

        private static void CampXmlSerializer(Camp a, string path)
        {
            XmlSerializer camXmlSerializer = new XmlSerializer(typeof(Camp));
            using (Stream xmlFileStream = new FileStream(path, FileMode.Create))
            {
                camXmlSerializer.Serialize(xmlFileStream, a);
            }
        }

    }
}
